**Integration Test Suite**

Generic framework for performance testing using Apache Jmeter.

Requirements:

* Apache Jmeter3.2 ([http://jmeter.apache.org/download_jmeter.cgi](Link URL))
* Java 8 (required for jmeter)